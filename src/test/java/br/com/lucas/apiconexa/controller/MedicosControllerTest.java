package br.com.lucas.apiconexa.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.lucas.apiconexa.AplicationConfingTest;
import br.com.lucas.apiconexa.dtos.MedicosNovo;
import br.com.lucas.apiconexa.repository.MedicosRepository;
import br.com.lucas.apiconexa.service.MedicosService;
 

@DisplayName("MedicosController")
public class MedicosControllerTest extends AplicationConfingTest {

    @MockBean
    private MedicosService medicosService;
    @MockBean
    private MedicosRepository repo;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void testSalva() throws JsonProcessingException, Exception {

        var medicosNovo = new MedicosNovo("lucas@gmail.com", "123456", "123456", "124.826.564-51", "24/11/1997",
                "(81)981232279", "TI");

        mockMvc.perform(post("/api/v1/signup")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(medicosNovo)))
                .andExpect(MockMvcResultMatchers.status().isCreated());

    }

}
