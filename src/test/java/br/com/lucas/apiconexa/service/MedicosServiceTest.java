package br.com.lucas.apiconexa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.UUID;

 
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
// import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import br.com.lucas.apiconexa.AplicationConfingTest;
import br.com.lucas.apiconexa.models.Medicos;
import br.com.lucas.apiconexa.repository.MedicosRepository;

@DisplayName("MedicosService")
public class MedicosServiceTest extends AplicationConfingTest {

        // @MockBean
        // private BCryptPasswordEncoder encoder;
        @MockBean
        private MedicosRepository repo;
        @Autowired
        private MedicosService service;

        @Test
        public void testeSalvarMedicos() {

                var medico = new Medicos(null, "email@gmail.com", "123456", "124.826.564-51", "24/11/1997",
                                "(81)9 81232279",
                                "TI");
                service.salvar(medico);

        }

        @Test
        public void testeExecaoEmailJaExite() {

                try {

                        var medico = new Medicos(null, "lucasteste@gmail.com", "123456", "124.826.564-51", "24/11/1997",
                                        "(81)9 81232279",
                                        "TI");
                        service.salvar(medico);
                        fail("esperava um erro");
                } catch (Exception e) {
                        assertEquals("O e-mail já existe no sistema.", e.getMessage());
                }

        }

        @BeforeEach
        public void setup() {
                var medico = new Medicos(UUID.randomUUID(), "lucasteste@gmail.com", "123456", "124.826.564-51",
                                "24/11/1997",
                                "(81)9 81232279",
                                "TI");
                Mockito.when(repo.findByEmail(medico.getEmail())).thenReturn(medico);
        }

}
