package br.com.lucas.apiconexa.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.lucas.apiconexa.models.Medicos;
import br.com.lucas.apiconexa.repository.MedicosRepository;
import br.com.lucas.apiconexa.service.exception.DatabaseException;
import br.com.lucas.apiconexa.service.exception.UniqueFieldException;
import lombok.var;

@Service
public class MedicosService {
    
    // @Autowired
	// private BCryptPasswordEncoder encoder;

    @Autowired
    private MedicosRepository repo;

    
    public Medicos salvar(Medicos medicos) {

        this.checadoreEmailJaInformado(medicos.getEmail());
  //      this.criptografaSenha(medicos);
        try {
            medicos = repo.save(medicos);
        } catch (Exception e) {
            throw new DatabaseException(e.getMessage());
        }
     

        return medicos;
    }

    // public void criptografaSenha(Medicos medicos){
    //     var senhaCriptografada =  encoder.encode(medicos.getSenha() );
    //     medicos.setSenha(senhaCriptografada);
    // }

    public void checadoreEmailJaInformado(String email) {
        var medico = repo.findByEmail(email);

        if (medico != null) {
            throw new UniqueFieldException("O e-mail já existe no sistema.");
        }
    }

}
