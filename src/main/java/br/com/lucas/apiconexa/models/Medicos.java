package br.com.lucas.apiconexa.models;

import java.util.UUID;

import javax.persistence.Entity;



import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
@Entity
public class Medicos extends Pessoa {
    private static final long serialVersionUID = 1L;

    private String especialidade;

    public Medicos(UUID id, String email, String senha, String cpf,
            String dataNascimento, String telefone, String especialidade) {
        super(id, email, senha, cpf, dataNascimento, telefone);
        this.especialidade = especialidade;
    }

}
