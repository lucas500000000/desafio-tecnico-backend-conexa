package br.com.lucas.apiconexa.models;

import java.util.UUID;

import javax.persistence.Entity;

import org.hibernate.validator.constraints.br.CPF;

 
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
 

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Entity
public class Pessoa extends Usuario {

    private static final long serialVersionUID = 1L;

    @CPF(message = "Informe um cpf valido")   
    private String cpf;
    private String dataNascimento;
    private String telefone;

    public Pessoa(UUID id, String email, String senha,String cpf,
            String dataNascimento, String telefone) {
        super(id, email, senha);
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
        this.telefone = telefone;
    }

    

}
