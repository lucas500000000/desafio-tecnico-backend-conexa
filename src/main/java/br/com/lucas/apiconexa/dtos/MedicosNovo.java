package br.com.lucas.apiconexa.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicosNovo {
    

    
    private String email;
    private String senha;
    private String confirmacaoSenha;
    private String cpf;
    private String dataNascimento;
    private String telefone;
    private String especialidade;






}
