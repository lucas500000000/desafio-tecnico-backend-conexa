package br.com.lucas.apiconexa.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;

import br.com.lucas.apiconexa.models.Usuario;

@NoRepositoryBean
public interface PessoaBaseRepository <T extends Usuario> extends JpaRepository<T,UUID> {
	Usuario findByEmail(String email);
}
