package br.com.lucas.apiconexa.repository;

import javax.transaction.Transactional;

import br.com.lucas.apiconexa.models.Medicos;

@Transactional
public interface MedicosRepository extends PessoaBaseRepository<Medicos> {
    public Medicos findByEmail(String email);
}
