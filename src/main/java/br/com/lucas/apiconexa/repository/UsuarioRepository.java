package br.com.lucas.apiconexa.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.lucas.apiconexa.models.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository< Usuario, UUID>  {
	Usuario findByEmail(String email);
}
