package br.com.lucas.apiconexa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiConexaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiConexaApplication.class, args);
		System.out.println("\n\n || Dificuldades preparam pessoas comuns para destinos extraordinários. 'As Crônicas de Nárnia' ||");
	}

}
